// ==UserScript==
// @name     Boutons Rabbit
// @version  8
// @grant    none
// @include http://localhost:15672/*
// @include https://socle-integ-admin-rabbitmq.samse.fr:1443/*
// @include https://socle-recette-admin-rabbitmq.samse.fr:1443/*
// @include https://socle-preprod-admin-rabbitmq.samse.fr:1443/*
// @include https://socle-prod-admin-rabbitmq.samse.fr:1443/*
// @include http://bao-socle-integ-admin-rabbitmq.samse.fr/*
// @include https://bao-socle-integ-admin-rabbitmq.samse.fr/*
// @include http://bao-socle-recette-admin-rabbitmq.samse.fr/*
// @include https://bao-socle-recette-admin-rabbitmq.samse.fr/*
// @include http://bao-socle-preprod-admin-rabbitmq.samse.fr/*
// @include https://bao-socle-preprod-admin-rabbitmq.samse.fr/*
// @include http://bao-socle-prod-admin-rabbitmq.samse.fr/*
// @include https://bao-socle-prod-admin-rabbitmq.samse.fr/*
// ==/UserScript==
function BoutonsRabbitScript() {

    /* ---- SESSION MANAGEMENT ----*/
    const activeSessionIdAttributeName = "buttons.active-session-id";
    let currentSessionId = Date.now() + '_' + Math.random().toString(36).substr(2);
    localStorage.setItem(activeSessionIdAttributeName, currentSessionId);

    function isCurrentSession() {
        return localStorage.getItem(activeSessionIdAttributeName) !== currentSessionId;
    }

    /* ---- SIMPLE EXECUTION POOL MANAGER TO AVOID FREEZING THE BROWSER ----*/
    var functionPoolManager = (function () {
        const MAX_PARALLEL_CALL = 150;
        var queue = [] //to store the functions to call
        var activeCall = 0

        function queueRequest(functionToCall) {
            queue.push(functionToCall)
            checkQueue()
        }

        function onPromiseCompleteOrFailure() {
            activeCall--
            checkQueue()
        }

        function checkQueue() {
            if (queue.length && activeCall <= MAX_PARALLEL_CALL) {
                let functionToCall = queue.shift()
                if (!functionToCall) {
                    return
                }

                activeCall++

                try {
                    functionToCall(onPromiseCompleteOrFailure);
                } catch (e) {
                    onPromiseCompleteOrFailure();
                }
            }
        }

        return {
            run: queueRequest,
        }
    })()

    /* -------- DATABASE -----*/
    const dbName = "RabbitButtons";
    const storeName = "messages"
    // Sur la ligne suivante, vous devez inclure les préfixes des implémentations que vous souhaitez tester.
    window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    // N'UTILISEZ PAS "var indexedDB = ..." si vous n'êtes pas dans une fonction.
    // De plus, vous pourriez avoir besoin de réferences à des objets window.IDB*:
    window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
    window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange
    // (Mozilla n'a jamais préfixé ces objets, donc nous n'avons pas besoin de window.mozIDB*)

    if (!window.indexedDB) {
        window.alert("Impossible d'utiliser IndexedDB (non disponible dans votre navigateur)")
        throw "No Indexed DB";
    }

    const waitTimeMessages = 500;
    const waitTimePage = 1000;
    const getMessageButtonText = 'Get Message(s)';
    const doneCssClass = "message-traite";

    const queueApiPrefix = "/queues/%2F/";


    const request = window.indexedDB.open(dbName, 3);
    var db;

    request.onerror = function (event) {
        alert("Impossible d'utiliser IndexedDB");
    };
    request.onsuccess = function (event) {
        db = event.target.result;

        db.onerror = function (event) {
            // Gestionnaire d'erreur générique pour toutes les erreurs de requêtes de cette base
            alert("Database error: " + event.target.errorCode);
            console.error("Database error", event);
        };
        console.log("Indexed DB loaded", db);
    };

    request.onupgradeneeded = function (event) {
        var db = event.target.result;


        var objectStore = db.createObjectStore(storeName, {keyPath: "id", autoIncrement: true});

        // Créer un index pour rechercher les messages par queue
        objectStore.createIndex("queue", "queue", {unique: false});

        // Créer un index pour rechercher les messages par action
        objectStore.createIndex("action", "action", {unique: false});


        console.log("DB init complete", db.name);
    };

    function transactionComplete(event) {
        console.log("Transaction complete");
    };

    function transactionError(event) {
        alert("Une erreur est survenue pendant la transaction");
        console.error("Transaction error", event);
    };

    /* --- CHECK IF REMAINING MESSAGES IN INDEXED DB --*/

    const promptableActions = ['delete', 'retry'];

    function checkLeftOverMessagesInDb() {
        var request = db.transaction([storeName]).objectStore(storeName).getAll();
        request.onerror = function (event) {
            alert("Erreur lors de la récupération des messages de la base locale");
            console.log("Erreur lors de la récupération des message de la base locale au démarrage.", event);
        };
        request.onsuccess = function (event) {
            const messages = request.result;
            if (messages.length > 0) {
                setNumberOfMessagesToPublishTotal(messages.length);
                messages.forEach(function (message) {

                    const doThisMessage = promptableActions.indexOf(message.action) < 0 || confirm(`⚠️⚠️⚠️⚠️⚠️ 𝗔𝗟𝗘𝗥𝗧𝗘 : 𝗜𝗠𝗣𝗢𝗥𝗧𝗔𝗡𝗧 ⚠️⚠️⚠️⚠️⚠️

Un message non traité a été trouvé dans votre navigateur. En voici les détails:

- Queue : ${message.queue}
- Date déclenchement de l'action : ${new Date(message.date).toLocaleString()}
- Action: ${message.action}
- Headers : ${JSON.stringify(message.message.properties.headers)}
- Payload :
---------------------------------------------------------------------------------------------------------
${message.message.payload}
---------------------------------------------------------------------------------------------------------

Documentation des actions:
- 🔁 "retry": le message va être déplacé vers la file non-DLQ correspondant à la DLQ et il sera donc consommé
- 🚮 "delete" : le message sera tout simplement supprimé
- ⏺️ "requeue" ou autre : le message sera replacé dans la DLQ.


Voulez vous rejouer l'action en question? (Si vous choisissez non, ça vous sera proposé à la prochain connexion sur l'interface Rabbit uniquement sur votre navigateur)
					`.trim())

                    if (doThisMessage) {
                        operationStart("Message Id=" + message.id);
                        functionPoolManager.run(doneCallback => doActionFromMessage(message, doneCallback));
                    } else {
                        operationEnd("Message Ignored Id=" + message.id);
                    }
                });
            }
        };
    }


    /* -------- FUNCTIONAL CODE -----*/


    function getQueueName() {
        return document.querySelector('#main > h1 > b').innerText;
    }

    function getExchangeName() {
        const elem = document.querySelector("form[action='#/exchanges/publish'] > input[name=name]");


        return elem ? elem.value : "amq.default";
    }


    window.operationInProgressCount = 0;

    function confirmExit() {
        if (operationInProgressCount > 0) {
            return "Une opération est en cours, merci de rester sur la page.";
        }
    }

    window.onbeforeunload = confirmExit;

    function operationStart(name) {
        console.log("Start Operation", name, window.operationInProgressCount, window.operationInProgressCount + 1);
        window.operationInProgressCount++;

        if (window.operationInProgressCount === 1) {
            showLoader();
        }
    }

    function operationEnd(name) {
        console.log("End Opération", name, window.operationInProgressCount, window.operationInProgressCount - 1);
        window.operationInProgressCount--;

        if (window.operationInProgressCount <= 0) {
            window.operationInProgressCount = 0;
            refreshMessages();
            hideLoader();
        }
    }

    function loaderBarElement() {
        return document.getElementById("loader-progress-bar");
    }

    function showLoader() {
        const loaderBar = loaderBarElement();
        loaderBar.style.width = "0%";
        loaderBar.innerText = "0%";
        document.body.classList.add("operation-running");
    }

    function hideLoader() {
        document.body.classList.remove("operation-running");
    }


    window.numberOfMessagesPublished = 0;
    window.numberOfMessagesToPublishTotal = 1;

    function setNumberOfMessagesToPublishTotal(numberOfMessages) {
        window.numberOfMessagesPublished = 0;
        window.numberOfMessagesToPublishTotal = numberOfMessages;
    }

    function messagePublished() {
        window.numberOfMessagesPublished++;
        const percentOfMessagesPublished = (window.numberOfMessagesPublished * 100 / window.numberOfMessagesToPublishTotal).toFixed(2) + "%";

        const loaderBar = loaderBarElement();
        loaderBar.style.width = percentOfMessagesPublished;
        loaderBar.innerText = percentOfMessagesPublished;
    }

    function refreshMessages() {
        const refreshMessagesButton = document.querySelector("form[action='#/queues/get'] input[type=submit]");
        if (refreshMessagesButton) {
            refreshMessagesButton.click();
        }
    }

    function newMessage(messageLine) {
        messageLine.classList.add(doneCssClass);
        const index = indexInsideParent(messageLine);
        messageLine.id = "message-" + index;

        const messageTable = messageLine.querySelector("table.facts > tbody");

        messageTable.innerHTML += messageActionsTemplate;

        const retryButton = messageTable.querySelector("button.retry-button");
        retryButton.id = "retry-button-" + index;
        retryButton.dataset.index = index;
        retryButton.addEventListener('click', retryMessage, false);

        const deleteButton = messageTable.querySelector("button.delete-button");
        deleteButton.id = "delete-button-" + index;
        deleteButton.dataset.index = index;
        deleteButton.addEventListener('click', deleteMessage, false);

    }

    function checkNewMessages() {
        document.querySelectorAll("#msg-wrapper > div.box:not(." + doneCssClass + ")").forEach(function (messageLine) {
            newMessage(messageLine);
        });
        checkNewMessagesCron = setTimeout(checkNewMessages, waitTimeMessages);
    }


    function pageLoaded() {
        console.log("Page is loaded, starting script");

        const queueName = getQueueName();

        checkLeftOverMessagesInDb();
        checkNewMessages();
    }

    let currentQueueName = null;
    let checkNewMessagesCron = null;

    function waitForPageLoad() {
        const queueNameElement = document.querySelector('#main > h1 > b');
        if (!queueNameElement || queueNameElement.innerText.indexOf('DLQ.') !== 0) {
            console.log("DLQ page not yet loaded, trying again in " + waitTimePage + "ms");
            if (checkNewMessagesCron) {
                clearTimeout(checkNewMessagesCron);
                checkNewMessagesCron = null;
                currentQueueName = null;
            }
        } else {
            const queueName = queueNameElement.innerText;
            if (queueName !== currentQueueName) {
                currentQueueName = queueName;
                console.log("DLQ page loaded triggered");
                pageLoaded();
            }
        }
        setTimeout(waitForPageLoad, waitTimePage);
    }

    waitForPageLoad();

    function indexInsideParent(elem) {
        var i = 0;
        while ((elem = elem.previousSibling) != null) {
            if (elem.nodeType === Node.ELEMENT_NODE) {
                ++i;
            }
        }
        return i;
    }

    const messageActionsTemplate = `
  <tr class="message-actions">
  <th>Actions</th>
  <td>
  <button  class="message-button retry-button">Retry message</button>
  <button class="message-button delete-button button-danger">Delete message</button>
  </td>
  </tr>`;

    function removeDLQ(dlqQueueName) {
        if (dlqQueueName.indexOf('DLQ.') === 0) {
            return dlqQueueName.substring(4);
        }

        throw "DLQ must start with 'DLQ.'. Got : " + dlqQueueName;
    }

    function removeMessageFromDb(messageId) {
        var request = db.transaction([storeName], "readwrite")
            .objectStore(storeName)
            .delete(messageId);
        request.onsuccess = function (event) {
            console.log("Message", messageId, "supprimé de la BD locale");
            operationEnd("Message Id=" + messageId)
        };

        request.onerror = function (event) {
            console.error("Erreur lors de la suppression du message", messageId, event);
            operationEnd("Message Id=" + messageId)
        };
    }

// copié de publish_msg0 de rabbit et adaptée pour avoir un callback
    function publish_msg1(params, callback) {
        var path = fill_path_template('/exchanges/:vhost/:name/publish', params);
        params['payload_encoding'] = 'string';
        params['properties'] = {};
        params['properties']['delivery_mode'] = parseInt(params['delivery_mode']);
        if (params['headers'] != '')
            params['properties']['headers'] = params['headers'];
        var props = [['content_type', 'str'],
            ['content_encoding', 'str'],
            ['correlation_id', 'str'],
            ['reply_to', 'str'],
            ['expiration', 'str'],
            ['message_id', 'str'],
            ['type', 'str'],
            ['user_id', 'str'],
            ['app_id', 'str'],
            ['cluster_id', 'str'],
            ['priority', 'int'],
            ['timestamp', 'int']];
        for (var i in props) {
            var name = props[i][0];
            var type = props[i][1];
            if (params['props'][name] != undefined && params['props'][name] != '') {
                var value = params['props'][name];
                if (type == 'int') value = parseInt(value);
                params['properties'][name] = value;
            }
        }
        with_req('POST', path, JSON.stringify(params), callback);
    }


    function publishMessage(messageId, queue, message, doneCallback) {
        publish_msg1({
            vhost: "/",
            routing_key: queue,
            delivery_mode: message.properties.delivery_mode,
            name: message.exchange == "" ? getExchangeName() : message.exchange,
            payload: message.payload,
            properties: {},
            props: message.properties,
            headers: message.properties.headers
        }, function (response) {
            var result = JSON.parse(response.responseText);
            if (result.routed) {
                console.log("Message", messageId, "routed to ", queue);
                removeMessageFromDb(messageId);
                messagePublished();
            } else {
                show_popup('warn', 'Message envoyé mais non routé (file Rabbit inexistante?). Le message reste stocké dans votre navigateur.');
            }
            doneCallback();
        });
    }

    function doActionFromMessage(message, doneCallback) {
        switch (message.action) {
            case 'delete':
                removeMessageFromDb(message.id);
                doneCallback();
                break;
            case 'retry':
                publishMessage(message.id, removeDLQ(message.queue), message.message, doneCallback);
                break;
            case 'requeue':
            default:
                publishMessage(message.id, message.queue, message.message, doneCallback);
        }

    }

    function doActionFromMessageId(messageId, doneCallback) {
        var request = db.transaction([storeName]).objectStore(storeName).get(messageId);
        request.onerror = function (event) {
            alert("Erreur lors de la récupération du message " + messageId);
            console.log("Erreur lors de la récupération du message. ", event);
        };
        request.onsuccess = function (event) {
            doActionFromMessage(request.result, doneCallback);
        };
    }


    function fetchMessages(action, indexToPerformActionOn) {
        const queueName = getQueueName();
        const date = Date.now();

        operationStart("fetchMessage");
        with_req('POST', queueApiPrefix + queueName + "/get", JSON.stringify({
            "count": Number.MAX_SAFE_INTEGER,
            "ackmode": "ack_requeue_false",
            "encoding": "auto"
        }), function (resp) {
            var transaction = db.transaction([storeName], "readwrite");
            transaction.oncomplete = transactionComplete;
            transaction.onerror = transactionError;

            let messageList = JSON.parse(resp.response);

            var objectStore = transaction.objectStore(storeName);

            setNumberOfMessagesToPublishTotal(messageList.length);

            for (let messageIndex = 0; messageIndex < messageList.length; messageIndex++) {
                operationStart("Message Idx=" + messageIndex);

                const actionForMessage = indexToPerformActionOn === messageIndex ? action : 'requeue';

                const message = {
                    queue: queueName,
                    action: actionForMessage,
                    date: date,
                    message: messageList[messageIndex]
                };
                var request = objectStore.add(message);
                request.onsuccess = function (event) {
                    const messageId = event.target.result;
                    console.log("Message saved id", messageId, "action", actionForMessage);

                    functionPoolManager.run(doneCallback => doActionFromMessageId(messageId, doneCallback));
                };
            }
            operationEnd("fetchMessage");

        });

    }

    function retryMessage(e) {
        actionOnClick(e, 'retry');
    }

    function deleteMessage(e) {
        actionOnClick(e, 'delete');
    }

    function getMessageCount() {
        return parseInt(document.querySelector("#message-0 > p > b").innerText) + 1;
    }

    function actionOnClick(e, actionLabel) {
        if (isCurrentSession()) {
            show_popup('warn', "Un autre onglet de l'interface d'administration RabbitMQ a été ouvert, merci de rafraichir la page et de refaire le " + actionLabel + ".");
            return;
        }


        if (operationInProgressCount > 0) {
            show_popup('warn', 'Une opération est déjà en cours, réessayez plus tard');
            return;
        }

        const messageCount = getMessageCount();

        if (messageCount > 500) {
            const noticeConfirmation = confirm(`La DLQ contient beaucoup de messages (${messageCount}) l'action ${actionLabel} va être assez lente.

☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️
Merci de ne pas fermer et/ou recharger votre navigateur et/ou l'onglet en cours jusqu'à la disparition de l'écran "Operation progress".
☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️ ☢️

Aussi, assurez-vous que personne d'autre ne manipule cette file DLQ pour éviter les conflits.

Voulez-vous continuer?!`);

            if (!noticeConfirmation) {
                return;
            }
        }


        const messageIndex = parseInt(e.target.dataset.index);
        const msgPayload = document.querySelector("#message-" + messageIndex + " .msg-payload").innerText;
        var confirmation = confirm("Voulez-vous " + actionLabel + " le message suivant:\n" + msgPayload);

        if (confirmation) {
            document.getElementById("msg-wrapper").innerHTML = "";

            console.log(actionLabel, messageIndex);
            fetchMessages(actionLabel, messageIndex);
        }
    }


    const styleCss = `
  button.message-button {
    display: inline-block;
  }

  button.button-danger {
    background-color: red;
  }

  button.button-danger:hover {
    background-color: #F60;
  }
  `;

    const styleElement = document.createElement("style");
    styleElement.innerHTML = styleCss;
    document.body.append(styleElement);

    const loaderHtml = `
<div id="loader-wrapper">
    <div id="loader"></div>
 		<div id="loader-progress">
			<h1>Operation progress</h1>
			<div class="progress-container">
				<div id="loader-progress-bar" class="progress-bar"></div>
      </div>
		</div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>
<style>

	body #loader-wrapper {
		display: none;
	}

  body.operation-running #loader-wrapper {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 1000;
        background-color: rgba(0,0,0,50%);
				display: block;
    }
    body.operation-running #loader {
        display: block;
        position: relative;
        left: 50%;
        top: 50%;
        width: 150px;
        height: 150px;
        margin: -75px 0 0 -75px;
        border-radius: 50%;
        border: 3px solid transparent;
        border-top-color: #3498db;

        -webkit-animation: spin 2s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
        animation: spin 2s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
    }

    body.operation-running #loader:before {
        content: "";
        position: absolute;
        top: 5px;
        left: 5px;
        right: 5px;
        bottom: 5px;
        border-radius: 50%;
        border: 3px solid transparent;
        border-top-color: #e74c3c;

        -webkit-animation: spin 3s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
        animation: spin 3s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
    }

    body.operation-running #loader:after {
        content: "";
        position: absolute;
        top: 15px;
        left: 15px;
        right: 15px;
        bottom: 15px;
        border-radius: 50%;
        border: 3px solid transparent;
        border-top-color: #f9c922;

        -webkit-animation: spin 1.5s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
          animation: spin 1.5s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
    }

    body.operation-running #loader-progress {
        display: block;
        position: relative;
        left: 10px;
        top: 10px;
        color: white;
        max-height: 200px;
        background-color: rgba(0,0,0,50%);
        max-width: 50%;
        padding: 20px;
    }

    body.operation-running .progress-container {
      width: 100%;
      background-color: grey;
    }

    body.operation-running .progress-bar {
      width: 0%;
      height: 30px;
      background-color: green;
      text-align: center;
      line-height: 30px;
      font-weight: bold;
			max-width: 100%;
    }

    @-webkit-keyframes spin {
        0%   {
            -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(0deg);  /* IE 9 */
            transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
        }
        100% {
            -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(360deg);  /* IE 9 */
            transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
        }
    }
    @keyframes spin {
        0%   {
            -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(0deg);  /* IE 9 */
            transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
        }
        100% {
            -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(360deg);  /* IE 9 */
            transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
        }
    }
</style>
`;

    document.body.innerHTML += loaderHtml;

}


var script = document.createElement("script");
script.type = "application/javascript";
script.textContent = "(" + BoutonsRabbitScript + ")();";

document.body.appendChild(script);
