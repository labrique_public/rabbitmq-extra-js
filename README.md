Projet qui regroupe les scripts utilisateur (Greasemonkey, Tampermonkey)

# Installation
## Firefox : Greasemonkey

Pour information, Tampermonkey est disponible sur Firefox également, si vous voulez l'utiliser plutôt que Greasemonkey, suivez les étapes prévus pour Chrome / Edge.

### Installer Greasemonkey (Firefox)
- Aller sur cette url : https://addons.mozilla.org/fr/firefox/addon/greasemonkey/
- Cliquer sur "Ajouter à Firefox"
- Terminez l'installation du plug-in

### Installation du script sur Greasemonkey (Firefox)
- Aller sur l'URL suivante du script en "Raw": https://gitlab.com/labrique_public/rabbitmq-extra-js/-/raw/master/rabbitmq-buttons.user.js
    - Si on vous demande de vous authentifier, faites-le puis réessayez, si vous n'avez pas de compte, créez-en un.
    - Si vous n'avez pas le droit de voir ce fichier, demandez à un développeur de vous-en donner les droits.
    - Si vous avez besoin d'utiliser le script se trouvant sur une branche, changer `master` vers le nom de la branche que vous souhaitez utiliser dans l'URL.
- Attendre que le timer arrive à 0, puis cliquer sur "Installer"

Bravo le script est installé ! 

### Mise à jour du script (Firefox)
- Le script se met à jour tout seul automatiquement au bout de 7 jours.
- Pour forcer la mise à jour:
    - Cliquer sur le singe en haut à droite (icone Greasemonkey).
    - Cliquer le nom du script "Boutons Rabbit".
    - Cliquer sur "Options du script utilisateur".
    - Cliquer sur "Mettre à jour maintenant".


## Chrome / Edge : Tampermonkey

### Installer Tampermonkey (Chrome / Edge)
- Aller sur cette url : https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=fr
- Si vous êtes sur Edge cliquer sur "Autoriser les extensions provenant d'autres magasins"
- Cliquer sur "Ajouter Chrome"
- Terminez l'installation du plug-in

### Installation du script sur Tampermonkey (Chrome / Edge)
- Aller sur l'URL suivante du script en "Raw": https://gitlab.com/labrique_public/rabbitmq-extra-js/-/raw/master/rabbitmq-buttons.user.js
    - Si on vous demande de vous authentifier, faites-le puis réessayez, si vous n'avez pas de compte, créez-en un.
    - Si vous n'avez pas le droit de voir ce fichier, demandez à un développeur de vous-en donner les droits.
    - Si vous avez besoin d'utiliser le script se trouvant sur une branche, changer `master` vers le nom de la branche que vous souhaitez utiliser dans l'URL.
- Cliquer sur le logo de Tampermonkey en haut à droite (une sorte de Robot à droite de la barre d'adresse)
- Cliquer sur "Tableau de Bord"
- Cliquer sur "Utilitaires"
- Coller le lien ci-dessous dans la barre "Install from URL"
- Cliquer sur "Installer"

### Mise à jour du script (Chrome / Edge)
- Le script se met à jour tout seul automatiquement au bout de 7 jours.
- Pour forcer la mise à jour:
    - Cliquer sur le logo de Tampermonkey en haut à droite (une sorte de Robot à droite de la barre d'adresse)
    - Aller dans l'onglet "Userscripts installés"
    - Cliquer sur "Boutons Rabbit"
    - Dans 'Editeur", cliquer sur "Fichier" puis "Vérifier les mises à jour"

## Internet Explorer
![Nope.](./docs/loki_ie.jpg)
